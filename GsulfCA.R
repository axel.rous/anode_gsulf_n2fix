library(tidyverse)
library(lubridate)
library(tis)
library(cowplot)
library(ggpubr)
library(igraph)


wd = "C:/Users/arous/Desktop/R for CA/"
{
plotfin=list(NA,NA,NA,NA,NA)
  tab_tot<-NULL
for(nb in list.files(paste(wd,"G.sulfurreducens/CA/",sep=""))[list.files(paste(wd,"G.sulfurreducens/CA/",sep=""))!="graphes"&list.files(paste(wd,"G.sulfurreducens/CA/",sep=""))!="multi"]){
  tabCA_alpha=NULL
  for(souche in list.files(paste(wd,"G.sulfurreducens/CA/",nb,"/",sep=""))){#[order(as.numeric(gsub("[^[:digit:]]","",substr(list.files("CA/souchepure/")[str_detect(list.files("CA/souchepure/"),"Desul|Xanthob")],5,6))))] # list of different reactor
    tabCA_beta1=NULL
    for(import in list.files(paste(wd,"G.sulfurreducens/CA/",nb,"/",souche,"/",sep=""))) {
        
        surface_WE = 25
        
        #Surface of one side of the Working electrode (1cm² per sampling at end of cycle indicated) if no sampling, "#" before {} in line 36 and un-comment line ... with correct surface
          
          tab = read.delim(paste(wd,"G.sulfurreducens/CA",nb,souche,import,sep="/"))
          tabCA_import <- tab |> 
           # filter(!str_detect(`X.I..mA`,"-")) |> 
            mutate(temps = (mdy_hms(substr(tab[,1],1,nchar(tab[,1])-5)))) |>
            #mutate(temps=as.numeric(gsub(",",".",time.s))) |> 
            mutate(Joule=NA) |> 
            #Date format with lubridate
            mutate(I = as.numeric(gsub(",",".",tab[,2]))) |> #numerical conversion of current intensity
            #mutate(puiss =  10^(as.numeric(paste(substr(tab[,2],20,20),substr(tab[,2],23,23),sep="")))) |> 
            mutate(J = I/(surface_WE/10)) |>#Current density from current intensity and WE surface
            mutate(puiss=as.numeric(gsub(",",".",P.W))) |> 
            select(temps,I,J,Joule,puiss)|> 
          mutate(Sample=paste(souche,nb,sep="_")) |> 
            filter(I>0)
          #Convert date format, current intensity in numeric and add current density and WE surface
          
          for(i in 2:length(rownames(tabCA_import))){
            tabCA_import$Joule[i]<-tabCA_import$puiss[i]*as.numeric(tabCA_import$temps[i]-tabCA_import$temps[i-1])*60
          }
    tabCA_beta1  <- rbind(tabCA_beta1,tabCA_import)
  }    
    tabCA_beta2<-tabCA_beta1   |>
      arrange(temps) |> 
      mutate(C=c(0,cumsum(lintegrate(as.numeric(temps),I/1000,xint=as.numeric(temps))))) |> 
      mutate(H2 = c(0,cumsum(lintegrate(as.numeric(temps),I,xint=as.numeric(temps))/(2*96485))))
    
     tabCA_alpha<-rbind(tabCA_alpha,tabCA_beta2)
     }  
  
  tab_tot<-rbind(tab_tot,mutate(tabCA_alpha,cycle=str_extract(import,"(?<=R)[:digit:]")))
  
  
      tabCA_outputb = tabCA_alpha|> 
        arrange(temps) 
        #mutate(filtre = quantile5(J)) |>
        #filter(filtre == 1) 
   
      
    #  tabCA_output= tabCA_outputb |> 
        # mutate(H2bis=case_when(!str_detect(Sample,"abio")~c(0,cumsum(lintegrate(as.numeric(temps),I-tabCA_mean$I[str_detect(tabCA_mean$Sample,"abio")],xint=as.numeric(temps))/(2*96485))),
        #                      str_detect(Sample,"abio")~c(0,cumsum(lintegrate(as.numeric(temps),I,xint=as.numeric(temps))/(2*96485))))) |> 
     #   select(-puiss)
      
      
      #write_csv2(tabCA_outputb,paste(wd,"/G.sulfurreducens/CA/multi/",nb,".csv",sep =""))
      
      
      tabCA_fin<-tabCA_outputb |> 
        #arrange(temps) |>
        mutate(cycle=str_extract(Sample,"(?<=batch )[:digit:]"))  |> 
        group_by(Sample,cycle) |> 
        mutate(Duree=as.numeric(interval(start=temps[1],end=temps))/3600/24) 
      
      
      
 
      
        
        
        pp <- ggplot(tabCA_fin)+
          aes(Duree,J)+
          xlab("Temps")+
          ylab("Densité de courant |J| (A/m²)")+
          #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
          geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30)) #courbe de regression(k = nb de points pour tracer la courde )
        p <- ggplot(tabCA_fin)+
          aes(Duree,J)+ 
          xlab("Temps")+
          ylab("Densité de courant |J| (A/m²)")+
          #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
          geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30))+
          theme_minimal(base_size=30)
        

          plot <- pp+ geom_point(aes(color=Sample),size = 2)+
            scale_color_manual(name="Reacteur",values=c("blue","green","lightblue","darkgreen"),label=c("N2-fix 1","Contrôle 1","N2-fix 2","Contrôle 2"))+
            ggtitle(nb)+
            theme_minimal(base_size=25)+theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5)))+xlab("Temps (jours)")+coord_cartesian(ylim=c(0,20))
          
          plotb <- p+ geom_point(aes(color=Sample),size = 2)+
            scale_color_manual(name="Reacteur",values=c("blue","green","lightblue","darkgreen"),label=c("N2-fix 1","Contrôle 1","N2-fix 2","Contrôle 2"))+
            ggtitle(nb)+xlab("Temps (jours)")
        
        
        
        ggsave(paste(wd,"/G.sulfurreducens/CA/graphes/",gsub(" ","",nb),".png",sep=""),plot = plot+
                 theme_minimal(base_size=25)+theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5))))
        ggsave(paste(wd,"/G.sulfurreducens/CA/graphes/b/",gsub(" ","",nb),".png",sep=""),plot = plotb+
                 theme_minimal(base_size=25)+theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5))))
pdf(paste("C:/Users/arous/Desktop/R for CA/G.sulfurreducens/CA/graphes/",gsub(" ","",nb),".pdf",sep=""))
print(plot)
print(plotb)
dev.off()        
   
plotfin[[as.numeric(str_extract(nb,"[:digit:]"))]]<-plot


}

  pdf("C:/Users/arous/Desktop/R for CA/G.sulfurreducens/CA/graphes/complet.pdf")
   print(plotfin[[1]])
   print(plotfin[[2]])
   print(plotfin[[3]])
   print(plotfin[[4]])
   print(plotfin[[5]])
dev.off()
}
tab_imp<-NULL
for(i in list.files("C:/Users/arous/Desktop/R for CA/G.sulfurreducens/CA/multi")){
  tab_imp_mani<-read.csv2(paste("C:/Users/arous/Desktop/R for CA/G.sulfurreducens/CA/multi/",i,sep="")) |> 
    mutate(batch=str_extract(Sample,"batch [:digit:]")) |> 
    mutate(Sample=str_extract(Sample,"R[:digit:]"))
tab_imp<-rbind(tab_imp,tab_imp_mani)
}
tab_tot_fin<-tab_tot |> 
  group_by(Sample) |> 
  summarize(apportJoule=sum(Joule))
tab_mani<-tab_imp|> 
  group_by(Sample,batch) |> 
  mutate(Duree=as.numeric(interval(start=temps[1],end=temps))/3600/24) |> 
  mutate(Sample=as.factor(Sample)) |> 
  mutate(batch=as.factor(batch)) |> 
mutate(temps=ymd_h(gsub("T","",str_extract(temps,".+(?>T)..")))) |>
group_by(Sample,batch) |>
summarise(J=mean(J)) |>
# select(-temps) |>
  mutate(SamBat=paste(Sample,batch,sep=""))
tab_mani



(max(tab_tot$C[str_detect(tab_tot$Sample,"R3")&!str_detect(tab_tot$Sample,"batch 1")]*0.98)-sum(c(6215.8,4554.9,5901,6034.8))+(4*30/1000*0.75*844100))/(20/14*0.75/1000)/1000000 #mJ/mol NH4 R1
(max(tab_tot$C[str_detect(tab_tot$Sample,"R1")&!str_detect(tab_tot$Sample,"batch 1")]*0.98)-sum(c(4886.7,1687.1,4756,3826.8))+(4*30/1000*0.75*844100))/(20/14*0.75/1000)/1000000 #MJ/mol NH4 R3

(max(tab_tot$C[str_detect(tab_tot$Sample,"R3")&!str_detect(tab_tot$Sample,"batch 1")]*0.98)-sum(c(6215.8,4554.9,5901,6034.8))+(4*30/1000*0.75*844100))/(1686/14*0.75/1000)/1000000 #mJ/mol N R1
(max(tab_tot$C[str_detect(tab_tot$Sample,"R1")&!str_detect(tab_tot$Sample,"batch 1")]*0.98)-sum(c(4886.7,1687.1,4756,3826.8))+(4*30/1000*0.75*844100))/(1686/14*0.75/1000)/1000000 #MJ/mol N R3  max de C car calcul d'intergral avec sum cumulés donc max supposé être le plus élevé par addition avec tous les autres 


by(tab_mani$J,tab_mani$Sample,shapiro.test)
var.test(tab_mani$J[tab_mani$Sample=="R4"],tab_mani$J[tab_mani$Sample=="R3"])


#by(tab_mani$J,tab_mani$Sample,t.test)



pairwise.t.test(tab_mani$J,tab_mani$Sample,var.equal=F)
t.test(tab_mani$J[tab_mani$Sample%in%c("R1","R3")],tab_mani$J[tab_mani$Sample%in%c("R2","R4")],paired=T)

##
import_rendement<-read_excel("~/Stage fin d'étude/Résultats/RésultatMEC.xlsx",sheet = "Rendements (2)", range = "AQ1:BC8")[-1:-2,-c(3,5,7,9,11,13)] |> 
  select(...1,CE,...4) |> 
  pivot_longer(cols=c(CE,...4),names_to="type",values_to="CE") |> 
  mutate(type=case_when(type%in%c("CE","rcat","rH2")~"Nfix",
                        !type%in%c("CE","rcat","rH2")~"tem")) |> 
  mutate(CE=as.numeric(CE))

by(import_rendement$CE,import_rendement$type,shapiro.test)
t.test(import_rendement$CE[import_rendement$type=="Nfix"],import_rendement$CE[import_rendement$type=="tem"],paired=T,var.equal=F)



CEGsulf<-data.frame(CE=c(54.97,101.88,92.79,85.25,119.98,71.99,125.16,93.42,92.55,109.23,63.17,111.65,85.86,106.99,99.83,79.18,107.66,93.60,87.73,93.06),rcat=c(53.97,36.65,26.11,49.39,46.61,62.41,43.69,85.82,66.16,55.91,68.09,51.21,72.82,65.22,61.87,68.27,49.68,46.06,62.83,50.86),rH2=c(29.67,37.34,24.22,42.1,55.92,44.93,54.68,80.17,61.23,61.07,43.01,57.18,65.52,69.78,61.76,54.06,53.49,43.11,55.12,50.86),Sample=c("R1","R1","R1","R1","R1","R2","R2","R2","R2","R2","R3","R3","R3","R3","R3","R4","R4","R4","R4","R4"),batch=c("batch 1","batch 2","batch 3","batch 4","batch 5","batch 1","batch 2","batch 3","batch 4","batch 5","batch 1","batch 2","batch 3","batch 4","batch 5","batch 1","batch 2","batch 3","batch 4","batch 5"),type=c("Nfix","tem","Nfix","tem","Nfix","tem","Nfix","tem","Nfix","tem","Nfix","tem","Nfix","tem","Nfix","tem","Nfix","tem","Nfix","tem"))
           by(CEGsulf$CE,CEGsulf$type,shapiro.test)
           by(CEGsulf$rcat,CEGsulf$type,shapiro.test)
           by(CEGsulf$rH2,CEGsulf$type,shapiro.test)
           var.test(CEGsulf$CE[CEGsulf$type=="Nfix"],CEGsulf$CE[CEGsulf$type=="tem"])
           var.test(CEGsulf$rcat[CEGsulf$type=="Nfix"],CEGsulf$rcat[CEGsulf$type=="tem"])
           var.test(CEGsulf$rH2[CEGsulf$type=="Nfix"],CEGsulf$rH2[CEGsulf$type=="tem"])
           
           
           t.test(CEGsulf$CE[CEGsulf$type=="Nfix"],CEGsulf$CE[CEGsulf$type=="tem"],paired=T,var.equal = T)
           t.test(CEGsulf$rcat[CEGsulf$type=="Nfix"],CEGsulf$rcat[CEGsulf$type=="tem"],paired=T,var.equal = T)
           t.test(CEGsulf$rH2[CEGsulf$type=="Nfix"],CEGsulf$rH2[CEGsulf$type=="tem"],paired=T,var.equal = T)




##CV
{
plot_tot<-NULL
for(nb in list.files(paste(wd,"G.sulfurreducens/CV/",sep=""))[list.files(paste(wd,"G.sulfurreducens/CV/",sep=""))!="graphes"&list.files(paste(wd,"G.sulfurreducens/CV/",sep=""))!="multi"]){
  tabCV_alpha=NULL
  for(souche in list.files(paste(wd,"G.sulfurreducens/CV/",nb,"/",sep=""))){#[order(as.numeric(gsub("[^[:digit:]]","",substr(list.files("CA/souchepure/")[str_detect(list.files("CA/souchepure/"),"Desul|Xanthob")],5,6))))] # list of different reactor
    tabCV_beta1=NULL
    test=0
    for(import in list.files(paste(wd,"G.sulfurreducens/CV/",nb,"/",souche,"/",sep=""))) {
      surface_WE = 25
      test<-test+1
      #Surface of one side of the Working electrode (1cm² per sampling at end of cycle indicated) if no sampling, "#" before {} in line 36 and un-comment line ... with correct surface
      
      tab = read.delim(paste(wd,"G.sulfurreducens/CV",nb,souche,import,sep="/"))
      tabCV_import <- tab |> 
        # filter(!str_detect(`X.I..mA`,"-")) |> 
        mutate(temps = (mdy_hms(substr(time.s,1,nchar(time.s)-5)))) |>
        #Date format with lubridate
        mutate(Ewe=as.numeric(gsub(",",".",Ewe.V))) |> 
        mutate(rep=as.character(as.numeric(gsub(",",".",cycle.number)))) |> 
        mutate(I = as.numeric(gsub(",",".",X.I..mA))) |> #numerical conversion of current intensity
        #mutate(puiss =  10^(as.numeric(paste(substr(tab[,2],20,20),substr(tab[,2],23,23),sep="")))) |> 
        mutate(J = I/(surface_WE/10)) |>#Current density from current intensity and WE surface
        select(temps,I,J,Ewe,rep)|> 
        mutate(tempsb=as.Date(temps)) |> 
        mutate(Sample=paste(souche,nb,sep="_")) |>
        nest(data=c(-rep,-tempsb)) |> 
        mutate(type=test)
        
      #Convert date format, current intensity in numeric and add current density and WE surface
      
      
     
        #Filter function to eliminate extrem value (quantile 5%), "#" if not necessary and in line 102
      tabCV_beta1  <- rbind(tabCV_beta1,tabCV_import) |> 
        arrange(tempsb)
    }  
    for(i in 1:length(tabCV_beta1$rep)){
      for(j in 1:length(tabCV_beta1$rep)){
        if(i!=j&tabCV_beta1$rep[i]==tabCV_beta1$rep[j]&tabCV_beta1$tempsb[i]<tabCV_beta1$tempsb[j]){
          tabCV_beta1$rep[j]=as.character(as.numeric(tabCV_beta1$rep[j])+as.numeric(max(tabCV_beta1$rep[tabCV_beta1$type==tabCV_beta1$type[i]])))
        }
        if(i!=j&tabCV_beta1$rep[i]==tabCV_beta1$rep[j]&tabCV_beta1$tempsb[j]<tabCV_beta1$tempsb[i]){
          tabCV_beta1$rep[i]=as.character(as.numeric(tabCV_beta1$rep[i])+as.numeric(max(tabCV_beta1$rep[tabCV_beta1$type==tabCV_beta1$type[j]])))
        }
      }
    }
    
    tabCV_beta2<-unnest(tabCV_beta1,data)   |>
      select(-tempsb,-type) |> 
      arrange(temps) 
    
    tabCV_alpha<-rbind(tabCV_alpha,tabCV_beta2)
  }  
  
  tabCV_outputb = tabCV_alpha|> 
    arrange(temps) 
  #mutate(filtre = quantile5(J)) |>
  #filter(filtre == 1) 
  
  
  #  tabCA_output= tabCA_outputb |> 
  # mutate(H2bis=case_when(!str_detect(Sample,"abio")~c(0,cumsum(lintegrate(as.numeric(temps),I-tabCA_mean$I[str_detect(tabCA_mean$Sample,"abio")],xint=as.numeric(temps))/(2*96485))),
  #                      str_detect(Sample,"abio")~c(0,cumsum(lintegrate(as.numeric(temps),I,xint=as.numeric(temps))/(2*96485))))) |> 
  #   select(-puiss)
  
  
  write_csv2(tabCV_outputb,paste(wd,"/G.sulfurreducens/CV/multi/",nb,".csv",sep =""))
  
  
  tabCV_fin<-tabCV_outputb |> 
    arrange(temps) |>
    mutate(cycle=str_extract(Sample,"(?<=batch )[:digit:]"))  
  
  
  
  
  
  
  
  pp <- ggplot(filter(tabCV_fin,rep==as.numeric(min(rep))+1))+
    aes(Ewe,J)+
    xlab("Ewe")+
    ylab("Densité de courant |J| (A/m²)")+
    #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
    geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30))+ #courbe de regression(k = nb de points pour tracer la courde )
    theme_minimal(base_size=30)+
    theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5)))
 p <- ggplot(filter(tabCV_fin,rep==max(rep)))+
    aes(Ewe,J)+ 
    xlab("Ewe")+
    ylab("Densité de courant |J| (A/m²)")+
    #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
    geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30))+
    theme_minimal(base_size=30)+
    theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5)))

 ppp <- ggplot(filter(tabCV_fin,rep==(as.numeric(max(rep))-1)/2))+
   aes(Ewe,J)+ 
   xlab("Ewe")+
   ylab("Densité de courant |J| (A/m²)")+
   #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
   geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30))+
   theme_minimal(base_size=30)+
   theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5)))
 
 pppp <- ggplot(filter(tabCV_fin,rep==(as.numeric(max(rep))-1)/2&str_detect(Sample,"R1")))+
   aes(Ewe,J)+ 
   xlab("Ewe")+
   ylab("Densité de courant |J| (A/m²)")+
   #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
   #geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30))+
   theme_minimal(base_size=30)+
   theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5)))
 
 
  
  plot <- pp+ geom_point(aes(color=Sample),size = 2)+
    scale_color_manual(name="Reacteur",values=c("blue","green","lightblue","darkgreen"),label=c("N2-fix 1","Contrôle 1","N2-fix 2","Contrôle 2"))+
    ggtitle(paste(nb,levels(as.factor(pp$data$rep)),sep="-"))+coord_cartesian(ylim=c(-2,12.5))
  
  plotb <- p+ geom_point(aes(color=Sample),size = 2)+
    scale_color_manual(name="Reacteur",values=c("blue","green","lightblue","darkgreen"),label=c("N2-fix 1","Contrôle 1","N2-fix 2","Contrôle 2"))+
    ggtitle(paste(nb,levels(as.factor(p$data$rep)),sep="-"))+coord_cartesian(ylim=c(-2,12.5))

  plotc <- ppp+ geom_point(aes(color=Sample),size = 2)+
    scale_color_manual(name="Reacteur",values=c("blue","green","lightblue","darkgreen"),label=c("N2-fix 1","Contrôle 1","N2-fix 2","Contrôle 2"))+
    ggtitle(paste(nb,levels(as.factor(ppp$data$rep)),sep="-"))+coord_cartesian(ylim=c(-2,12.5))
  
  plotd <- pppp+ geom_point(aes(color=Sample),size = 1)+
    scale_color_manual(name="Reacteur",values=c("black"),label=c("N2-fix 1"))+
    ggtitle(paste(nb,levels(as.factor(pppp$data$rep)),sep="-"))+coord_cartesian(ylim=c(-2,12.5))
  
  
  ggsave(paste(wd,"/G.sulfurreducens/CV/graphes/",gsub(" ","",nb),".png",sep=""),plot = plot+
           theme_minimal(base_size=25)+theme(legend.position="bottom"))
  ggsave(paste(wd,"/G.sulfurreducens/CV/graphes/b/",gsub(" ","",nb),".png",sep=""),plot = plotb+
           theme_minimal(base_size=25)+theme(legend.position="bottom"))
  ggsave(paste(wd,"/G.sulfurreducens/CV/graphes/c/",gsub(" ","",nb),".png",sep=""),plot = plotc+
           theme_minimal(base_size=25)+theme(legend.position="bottom"))
  ggsave(paste(wd,"/G.sulfurreducens/CV/graphes/d/",gsub(" ","",nb),".png",sep=""),plot = plotd+
           theme_minimal(base_size=25)+theme(legend.position="bottom"))
  pdf(paste("C:/Users/arous/Desktop/R for CA/G.sulfurreducens/CV/graphes/",gsub(" ","",nb),".pdf",sep=""))
  plot<-plot+guides(color = guide_legend(override.aes = list(size = 10)))+theme(legend.position="right")
  print(plot+theme(legend.position = "none"))
  print(plotb+theme(legend.position = "none"))
  print(plotc+theme(legend.position = "none"))
  print(plotd+theme(legend.position = "none"))
  print(as_ggplot(get_legend(plot)))
  dev.off()        
  
  if(as.numeric(str_extract(nb,"[:digit:]"))<=4){
  plot_tot[[as.numeric(str_extract(nb,"[:digit:]"))]]<-list(plot+theme(legend.position="none"),plotb+theme(legend.position="none"),plotc+theme(legend.position="none"))
  }
  if(as.numeric(str_extract(nb,"[:digit:]"))==5){
    plot_tot[[as.numeric(str_extract(nb,"[:digit:]"))]]<-list(plot+theme(legend.position="none"),plotb+theme(legend.position="none"),plotb+theme(legend.position="bottom"))
  }
}

pdf(paste("C:/Users/arous/Desktop/R for CA/G.sulfurreducens/CV/graphes/complet.pdf",sep=""))
print(plot_tot[[1]][[1]])
print(plot_tot[[1]][[2]])
print(plot_tot[[2]][[1]])
print(plot_tot[[2]][[2]])
print(plot_tot[[3]][[1]])
print(plot_tot[[3]][[2]])
print(plot_tot[[4]][[1]])
print(plot_tot[[4]][[2]])
print(plot_tot[[5]][[1]])
print(plot_tot[[5]][[2]])
dev.off()        

}
